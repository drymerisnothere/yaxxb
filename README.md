# Yet Another Xmpp matriX Bridge

This is a simple bridge between two rooms, one on a xmpp server and the other on a matrix server. Clone the repository, create a config file per bridge and pass it to the script:

    git clone https://daemons.cf/cgit/yaxxb
	cd yaxxb
	su -c "pip3 install -r requirements.txt"
	python3 yaxxb.py whatever.ini

Most of the code is taken from [jabbergram](https://daemons.cf/cgit/jabbergram/about/).
