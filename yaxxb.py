#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import sleekxmpp
import configparser
from threading import Thread
from sys import argv, exit
from sleekxmpp.xmlstream.stanzabase import ElementBase
from xml.dom import minidom
from matrix_client.client import MatrixClient
from matrix_client.api import MatrixRequestError


class Request(ElementBase):
    namespace = 'urn:xmpp:http:upload'
    name = 'request'
    plugin_attrib = 'request'
    interfaces = set(('filename', 'size'))
    sub_interfaces = interfaces


class Yaxxb(sleekxmpp.ClientXMPP):
    def __init__(self, xmpp_jid, xmpp_password, xmpp_room, nick, matrix_jid,
                 matrix_password, matrix_room):
        # XMPP
        super(Yaxxb, self).__init__(xmpp_jid, xmpp_password)
        self.add_event_handler('session_start', self.start)
        self.add_event_handler('groupchat_message', self.muc_message)

        self.xmpp_room = xmpp_room
        self.nick = nick
        self.xmpp_users = []
        self.xmpp_jid = xmpp_jid

        self.add_event_handler("muc::%s::got_online" % self.xmpp_room,
                               self.muc_online)
        self.add_event_handler("muc::%s::got_offline" % self.xmpp_room,
                               self.muc_offline)

        # Matrix
        self.matrix_jid = matrix_jid
        matrix = MatrixClient('https://' + matrix_jid.split(':')[1])
        matrix.login_with_password(username=matrix_jid.split(':')[0][1:],
                                   password=matrix_password)

        self.matrix_users = []

        try:
            self.matrix_room = matrix.join_room(matrix_room)
        except MatrixRequestError as e:
            print(e)
            if e.code == 400:
                print("Matrix room ID/Alias in the wrong format")
                exit(11)
            else:
                print('Creating room.')
                self.matrix_room = matrix.create_room(matrix_room)

        self.matrix_room.add_listener(self.matrix_message)
        matrix.start_listener_thread()

        # initialize http upload on a thread
        t = Thread(target=self.init_http)
        t.daemon = True
        t.start()

        print('Please wait a couple of minutes until it\'s correctly '
              'connected')

    def init_http(self):
        self.http_upload = self.HttpUpload(self)
        self.component = self.http_upload.discovery()

        if self.component:
            xml = self.http_upload.disco_info(self.component)
            xml = minidom.parseString(str(xml))
            self.max_size = int(xml.getElementsByTagName('value')
                                [1].firstChild.data)
        else:
            try:
                self.component = self.xmpp_jid.split('@')[1]
                xml = self.http_upload.disco_info(self.component)
                xml = minidom.parseString(str(xml))
                self.max_size = int(xml.getElementsByTagName('value')
                                    [1].firstChild.data)
            except:
                self.max_size = None

    def matrix_message(self, room, event):
        if event['sender'] != self.matrix_jid:
            msg = ''

            if event['type'] == "m.room.message":
                if event['sender'] not in self.matrix_users:
                    self.matrix_users.append(event['sender'] + ' ')

                if event['content']['msgtype'] == "m.text":
                    if event['content']['body'] == '.users':
                        self.say_users('xmpp')

                    else:
                        msg = "[ {0} ] {1}".format(event['sender'],
                                                   event['content']['body'])

                elif event['content']['msgtype'] == 'm.file':
                    size = event['content']['info']['size']
                    if self.max_size >= size:
                        url = 'https://' + self.matrix_jid.split(':')[1] + \
                              '/_matrix/media/v1/download/' + \
                              event['content']['url'].replace('mxc://', '')
                        req = requests.get(url)
                        url = self.http_upload.upload(self.component, '',
                                                      req.text, size)
                        msg = 'A file has being uploaded by ' + \
                              event['sender'] + ': ' + url
                    else:
                        msg = 'A file has being sent from matrix, but it\'s ' \
                              'too big'

                else:
                    print('Mensaje enviado que no es texto: ' + str(event))

            else:
                print('Evento raro: ' + str(event))

            self.send_message(mto=self.xmpp_room, mbody=msg, mtype='groupchat')

    def start(self, event):
        self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].joinMUC(self.xmpp_room, self.nick, wait=True)

    def muc_message(self, msg):
        if msg['body'] == '.users':
            self.say_users('matrix')

        elif msg['mucnick'] != self.nick:
            message = '[ ' + str(msg['from']).split('/')[1] + ' ] ' + \
                      str(msg['body'])
            self.matrix_room.send_text(message)

    def muc_online(self, presence):
        user = presence['muc']['nick']

        if user != self.nick:
            self.xmpp_users.append(presence['muc']['nick'])

    def muc_offline(self, presence):
        user = presence['muc']['nick']

        if user != self.nick:
            self.xmpp_users.remove(presence['muc']['nick'])

    def say_users(self, service):
        if service == 'xmpp':
            xmpp_users = ''
            for i in self.xmpp_users:
                xmpp_users = xmpp_users + ' ' + i
                message = 'XMPP Users:' + xmpp_users

            self.matrix_room.send_text(message)

        elif service == 'matrix':
            message = 'Matrix Users: ' + ''.join(self.matrix_users)
            self.send_message(mto=self.xmpp_room, mbody=message,
                              mtype='groupchat')

    class HttpUpload():
        def __init__(self, parent_self):
            self.parent_self = parent_self

        def discovery(self):
            disco = sleekxmpp.basexmpp.BaseXMPP.Iq(self.parent_self)
            disco['query'] = "http://jabber.org/protocol/disco#items"
            disco['type'] = 'get'
            disco['from'] = self.parent_self.xmpp_jid
            disco['to'] = self.parent_self.xmpp_jid.split('@')[1]

            d = disco.send(timeout=30)
            xml = minidom.parseString(str(d))
            item = xml.getElementsByTagName('item')

            for component in item:
                component = component.getAttribute('jid')
                info = self.disco_info(component)
                if "urn:xmpp:http:upload" in info:
                    http_upload_component = component
                    break
                else:
                    http_upload_component = ""

            return http_upload_component

        def disco_info(self, component):
            info = sleekxmpp.basexmpp.BaseXMPP.Iq(self.parent_self)
            info['query'] = "http://jabber.org/protocol/disco#info"
            info['type'] = 'get'
            info['from'] = self.parent_self.xmpp_jid
            info['to'] = component

            return str(info.send(timeout=30))

        def upload(self, component, verify_ssl, u_file, size):
            peticion = Request()
            peticion['filename'] = u_file.split('/')[-1]
            peticion['size'] = str(size)

            iq = sleekxmpp.basexmpp.BaseXMPP.Iq(self.parent_self)
            iq.set_payload(peticion)
            iq['type'] = 'get'
            iq['to'] = component
            iq['from'] = self.parent_self.xmpp_jid

            send = iq.send(timeout=30)

            xml = minidom.parseString(str(send))
            put_url = xml.getElementsByTagName('put')[0].firstChild.data

            verify_ssl = ''
            if verify_ssl == 'False':
                requests.put(put_url, data=u_file,
                             verify=False)
            else:
                requests.put(put_url, data=u_file)

            return put_url


if __name__ == '__main__':

    # parse config
    config = []
    parser = configparser.SafeConfigParser()

    if len(argv) == 2:
        parser.read(argv[1])
    else:
        parser.read('config.ini')

    for name, value in parser.items('config'):
        config.append(value)

    # assign values for the bot
    xmpp_jid = config[0]
    xmpp_password = config[1]
    xmpp_room = config[2]
    nick = config[3]
    matrix_jid = config[4]
    matrix_password = config[5]
    matrix_room = config[6]

    xmpp = Yaxxb(xmpp_jid, xmpp_password, xmpp_room, nick, matrix_jid,
                 matrix_password, matrix_room)
    xmpp.register_plugin('xep_0045')

    if xmpp.connect():
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")

    # Vols un gram nen?
